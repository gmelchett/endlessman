/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017-2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#define MAND_DEF_WIDTH 3.5
#define MAND_DEF_HEIGHT 2.0
#define MAND_DEF_X -2.5
#define MAND_DEF_Y -1.0
#define DEFAULT_MAX_ITERATIONS 150

struct mandelbrot_point {
	double x, y;
	double iteration;
};

struct mandelbrot_uncertain {
	double x, y, px, py;
	unsigned int idx;
	double iteration;
};

struct mandelbrot {
	double x;
	double width;
	double y;
	double height;
	unsigned int image_width;
	unsigned int image_height;
	bool ok;
	unsigned int depth_min;
	unsigned int depth_max;
	unsigned int start_iteration;
	double max_iteration;
	unsigned int dull;
	unsigned int never;
	unsigned int p_len;
	struct mandelbrot_point *p;
	unsigned int num_uncertain;
	struct mandelbrot_uncertain *u;
	double *mandelbrot;
};

struct mandelbrot *mandelbrot_alloc(int image_width, int image_height);
void mandelbrot_free(struct mandelbrot *p);
void mandelbrot(struct mandelbrot *p);
void mandelbrot_increase_depth(struct mandelbrot *m);
