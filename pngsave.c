
/* This probably a copy of some example code from the internet */

#include <png.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>

#define MAX_FLEN 128

extern bool verbose;

void png_save_as(char *fname, unsigned char *buffer, int width, int height)
{
	static int saved_images = 0;
	unsigned char *row;
	FILE *fp;
	png_structp png_ptr;
	png_infop info_ptr;

	fp = fopen(fname, "wb");

	if (fp == NULL) {
		fprintf(stderr, "Fail to open file: %s\n", fname);
		exit(1);
	}

	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	info_ptr = png_create_info_struct(png_ptr);
	setjmp(png_jmpbuf(png_ptr));
	png_init_io(png_ptr, fp);
	png_set_IHDR(png_ptr, info_ptr, width, height,
		     8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
		     PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	png_write_info(png_ptr, info_ptr);

	row = (png_bytep) malloc(3 * width * sizeof(png_byte));

	for (int y = 0 ; y < height ; y++) {
		for (int x = 0 ; x < width ; x++) {
			row[x * 3 + 0] = buffer[3*y * width + 3*x];
			row[x * 3 + 1] = buffer[3*y * width + 3*x +1];
			row[x * 3 + 2] = buffer[3*y * width + 3*x +2];
		}
		png_write_row(png_ptr, row);
	}

	png_write_end(png_ptr, NULL);
	free(row);
	fclose(fp);
	png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
	png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

	saved_images++;
	if (verbose)
		printf(" -- Image %d saved mandelbrot as %s\n", saved_images, fname);
}
