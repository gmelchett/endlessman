/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017-2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdbool.h>
#include <time.h>
#include <stdarg.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "colorize.h"
#include "mandelbrot.h"
#include "pngsave.h"
#include "storage.h"

#define MAX_ATTEMPTS 10
#define MAX_FLEN 128

static TTF_Font *font;
static SDL_Window *window;
static SDL_Surface *surface;

static void draw(unsigned char *buffer, int width, int height)
{
	SDL_Surface *image;
	image = SDL_CreateRGBSurfaceFrom(buffer, width, height, 24, width * 3,
					 0x000000ff,
					 0x0000ff00,
					 0x00ff0000,
					 0);
	SDL_BlitSurface(image, NULL, surface, NULL);
	SDL_FreeSurface(image);
	SDL_UpdateWindowSurface(window);
}

#define MSG_MAX 1024
static void print(int x, int y, const char *format, ...)
{
	SDL_Surface *image;
	SDL_Color textColor = {255, 255, 255, 0};
	SDL_Rect rect = {.x = x, .y = y};
	char msg[MSG_MAX];

	va_list args;
	va_start(args, format);
	vsnprintf(msg, MSG_MAX-1, format, args);
	va_end(args);

	image = TTF_RenderText_Solid(font, msg, textColor);
	SDL_BlitSurface(image, NULL, surface, &rect);

	SDL_FreeSurface(image);
	SDL_UpdateWindowSurface(window);
}

static void status(struct mandelbrot *m, unsigned int gray)
{
	print(100,100, "dull %d min: %d max: %d p: %d%% uncertain:%d%% (%d) never: %d gray: %d",
	      m->dull, m->depth_min, m->depth_max, 100*m->p_len/(m->image_width*m->image_height),
	      100*m->num_uncertain/(m->image_width*m->image_height), m->num_uncertain, m->never,
	      100*gray/(m->image_width*m->image_height));
}

static bool show_info = false;

static void load_endl(char *fname, struct mandelbrot *current, unsigned char *buffer, int width, int height)
{
	struct palette *pal;
	unsigned int gray;

	storage_load(fname, current, &pal);
	mandelbrot(current);
	colorize_paint_palette(pal);
	gray = colorize_mandelbrot(buffer, current, pal, show_info);
	colorize_free(pal);
	draw(buffer, width, height);
	if (show_info)
		status(current, gray);
}

bool verbose = true;

int main(int argc, char **argv)
{
	bool done = false;
	SDL_Event event;
	bool new_image = true;
	int previous_attempts = 0;
	struct palette *pal;
	unsigned int gray;
	bool from_args = false;
	int curr_args = 1;
	int width, height;
	unsigned char *buffer;
	struct mandelbrot *master, *previous, *current;

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();

	font = TTF_OpenFont("FreeSans.ttf", 18);

	window = SDL_CreateWindow("Endless Mandelbrot",
				  SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
				  0, 0,
				  SDL_WINDOW_FULLSCREEN_DESKTOP);

	if (window == NULL) {
		fprintf(stderr, "CreateWindow failed: %s\n", SDL_GetError());
		return 1;
	}
	SDL_ShowCursor(0);

	surface = SDL_GetWindowSurface(window);

	width = surface->w;
	height = surface->h;

	/* About in the middle */
	print(width/2 - 150, height/2 - 20, "Generating initial fractal..");

	srand(time(NULL));
	buffer = malloc(3 * width * height);

	master = mandelbrot_alloc(width, height);
	previous = mandelbrot_alloc(width, height);
	current = mandelbrot_alloc(width, height);

	mandelbrot(master);
	pal = colorize_create_palette(master, 0);
	gray = colorize_mandelbrot(buffer, master, pal, show_info);

	if (argc > 1) {
		load_endl(argv[curr_args], current, buffer, width, height);
		new_image = false;
		from_args = true;
		curr_args++;
	}

	while (!done) {

		if (new_image) {
			if (from_args) {
				if (curr_args == argc) {
					done = true;
					continue;
				}

				load_endl(argv[curr_args], current, buffer, width, height);
				new_image = false;
				curr_args++;

				continue;
			}

			double d = (double)(random() % 10 + 1);

			if (current->ok || previous->ok) {
				if (current->ok && current->p_len > 0) {
					struct mandelbrot *tmp = previous;
					previous = current;
					current = tmp;
					previous_attempts = 0;
				} else {
					previous_attempts++;
					if (previous_attempts == MAX_ATTEMPTS) {
						previous->ok = false;
						continue;
					}
				}
				int n = random() % previous->p_len;
				current->width = d * previous->width / (double)width;
				current->height = d * previous->height / (double)width;
				current->x = previous->p[n].x;
				current->y = previous->p[n].y;
				current->max_iteration = DEFAULT_MAX_ITERATIONS;
			} else {
				int n = random() % master->p_len;
				current->width = d * master->width / (double)width;
				current->height = d * master->height / (double)width;
				current->x = master->p[n].x;
				current->y = master->p[n].y;
				current->max_iteration = DEFAULT_MAX_ITERATIONS;
			}
			mandelbrot(current);

			if (current->ok) {

				for (int i = 0 ; i < 5; i++) {
					if (current->num_uncertain > 10 && random() % 3 == 0) {
						current->max_iteration += 50;
						mandelbrot_increase_depth(current);
					}
				}

				colorize_free(pal);
				pal = colorize_create_palette(current, 0);
				gray = colorize_mandelbrot(buffer, current, pal, show_info);

				draw(buffer, width, height);
				if (show_info)
					status(current, gray);
				new_image = false;
			}
		}

		while (SDL_PollEvent(&event)) {
			int k = 0;
			switch (event.type) {
			case SDL_KEYUP:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
				case SDLK_q:
					done = true;
					break;
				case SDLK_1:
					k = 2;
				case SDLK_2:
					if (k == 0)
						k = 3;
				case SDLK_3:
					if (k == 0)
						k = 4;
				case SDLK_4:
					if (k == 0)
						k = 5;
				case SDLK_5:
					if (k == 0)
						k = 6;
					colorize_free(pal);
					pal = colorize_create_palette(current, k);
					gray = colorize_mandelbrot(buffer, current, pal, show_info);

					draw(buffer, width, height);
					if (show_info)
						status(current, gray);
					break;
				case SDLK_i:
					show_info = !show_info;
					colorize_free(pal);
					pal = colorize_create_palette(current, 0);
					gray = colorize_mandelbrot(buffer, current, pal, show_info);
					draw(buffer, width, height);
					if (show_info)
						status(current, gray);
					break;
				case SDLK_d:
					current->max_iteration += 50;
					int uncertain_before = current->num_uncertain;
					mandelbrot_increase_depth(current);
					colorize_free(pal);
					pal = colorize_create_palette(current, 0);
					gray = colorize_mandelbrot(buffer, current, pal, show_info);
					draw(buffer, width, height);
					if (show_info) {
						print(100,50, "d uncertain:%d %% (%d)", 
						      100 *(uncertain_before - current->num_uncertain) /uncertain_before, 
						      uncertain_before - current->num_uncertain);
						status(current, gray);
					}

					break;
				case SDLK_s:
				{
					char fname[MAX_FLEN];

					storage_generate_unique_filename(current, fname, MAX_FLEN, ".png");
					png_save_as(fname, buffer, width, height);
					break;
				}
				case SDLK_e:
					storage_save(current, pal, NULL);
					break;
				case SDLK_SPACE:
					new_image = true;
					break;
				default:
					break;
				}
				break;
			case SDL_FINGERUP:
				new_image = true;
				break;
			case SDL_QUIT:
				done = true;
				break;
			default:
				break;
			}
		}
		SDL_Delay(200);
	}
	colorize_free(pal);

	mandelbrot_free(current);
	mandelbrot_free(master);
	mandelbrot_free(previous);
	free(buffer);

	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
