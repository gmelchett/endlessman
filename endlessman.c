/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017-2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/times.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <sys/resource.h>
#include <limits.h>
#include <libgen.h>

#include "mandelbrot.h"
#include "colorize.h"
#include "pngsave.h"
#include "storage.h"

#ifndef NZERO
#define NZERO 20
#endif

#define DEFAULT_WIDTH 1920
#define DEFAULT_HEIGHT 1080
#define MAX_ATTEMPTS 10
#define MAX_FLEN 1024

bool verbose = false;

static int toint(char *optarg)
{
	char *endptr = NULL;
	int val = strtol(optarg, &endptr, 10);
	if (errno != 0 || optarg == endptr) {
		fprintf(stderr, "Not a number `%s'.\n", optarg);
		exit(1);
	}
	return val;
}

int main(int argc, char **argv)
{
	unsigned char *buffer;
	int saved_images = 0;
	int random_seed = time(NULL);
	unsigned int num_images = 0;
	int width = DEFAULT_WIDTH;
	int height = DEFAULT_HEIGHT;
	bool everlasting = true;
	int previous_attempts = 0;
	struct palette *pal;
	bool quiet = false;
	char *output_file = NULL;
	int max_iterations = DEFAULT_MAX_ITERATIONS;
	char filename_copy[MAX_FLEN];
	char *path = NULL;

	struct tms start, end;
	int c;
	struct mandelbrot *master, *previous, *current;

	opterr = 0;

	while ((c = getopt (argc, argv, "hi:n:pqs:vx:y:o:")) != -1) {
		errno = 0;
		switch ((char)c) {
		case 'h':
			printf("\nUsage: %s [options]\n\n", argv[0]);
			printf(" -i n\t\tMax iteration depth. Default is 150.\n");
			printf(" -n n\t\tNumber of images to generate. Default no limit.\n");
			printf(" -o <filename>\tGenerate one fractal (png+endl)\n");
			printf(" -p\t\tRun at highest nice level\n");
			printf(" -q\t\tSilence\n");
			printf(" -s n\t\tRandom seed.\n");
			printf(" -v\t\tVerbose printing\n");
			printf(" -x n\t\tImage width. Default 1920\n");
			printf(" -y n\t\tImage height. Default 1080\n");
			printf("\n");
			exit(0);
			break;
		case 'i':
			max_iterations = toint(optarg);
			if (max_iterations < 2) {
				fprintf(stderr, "Max iterations too small.\n");
				return 1;
			}
			if (max_iterations > 1024) {
				fprintf(stderr, "Max iterations too big.\n");
				return 1;
			}

			break;

		case 'n':
			num_images = toint(optarg);
			everlasting = false;
			break;
		case 'o':
			num_images = 1;
			everlasting = false;
			output_file = optarg;
			/* Since dirname might alter its input */
			strncpy(filename_copy, output_file, MAX_FLEN-1);
			path = dirname(filename_copy);

			break;
		case 'p':
			setpriority(PRIO_PROCESS, getpid(), NZERO*2-1);
			break;
		case 'q':
			quiet = true;
			break;
		case 's':
			random_seed = toint(optarg);
			break;
		case 'v':
			verbose = true;
			break;
		case 'x':
			width = toint(optarg);
			break;
		case 'y':
			height = toint(optarg);
			break;

		case '?':
			if (optopt == 'h' || optopt == 'w')
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint (optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr,
					"Unknown option character `\\x%x'.\n",
					optopt);
			return 1;
		default:
			abort ();
		}
	}

	if (width < 10 || height < 10) {
		fprintf(stderr, "Requested image size too small.\n");
		return 1;
	}

	if (width > 4096 || height > 4096) {
		fprintf(stderr, "Cowardly refusing to create enormous fractals.\n");
		return 1;
	}

	srand(random_seed);

	buffer = malloc(3 * width * height);

	master = mandelbrot_alloc(width, height);
	previous = mandelbrot_alloc(width, height);
	current = mandelbrot_alloc(width, height);

	if (verbose)
		printf(" ** Generate original base Mandelbrot fractal\n");
	else if (!quiet) {
		char *msg = "Generating fractals";
		write(STDOUT_FILENO, msg, strlen(msg));
		fsync(STDOUT_FILENO);
	}

	mandelbrot(master);

	while (everlasting || saved_images < num_images) {

		double d = (double)(random() % 10 + 1);

		if (current->ok || previous->ok) {
			if (current->ok && current->p_len > 0) {
				struct mandelbrot *tmp = previous;
				previous = current;
				current = tmp;
				previous_attempts = 0;
			} else {
				previous_attempts++;
				if (previous_attempts == MAX_ATTEMPTS) {
					previous->ok = false;
					if (verbose)
						printf(" -- Restarting from root fractal\n");
					continue;
				}
			}
			int n = random() % previous->p_len;
			current->width = d * previous->width / (double)width;
			current->height = d * previous->height / (double)width;
			current->x = previous->p[n].x;
			current->y = previous->p[n].y;
			current->max_iteration = max_iterations;
		} else {
			int n = random() % master->p_len;
			current->width = d * master->width / (double)width;
			current->height = d * master->height / (double)width;
			current->x = master->p[n].x;
			current->y = master->p[n].y;
			current->max_iteration = max_iterations;
		}
		if (verbose)
			printf(" ** Start generating new fractal\n");

		times(&start);
		mandelbrot(current);
		times(&end);
		if (verbose) 
			printf("\t-- Generation took: %lu\n", end.tms_utime - start.tms_utime);

		if (current->ok) {

			/* Sometimes a fractal gets more intresting when increasing depth */
			for (int i = 0 ; i < 2; i++) {
				if (current->num_uncertain > 10 && random() % 3 == 0) {
					current->max_iteration += 50;
					mandelbrot_increase_depth(current);
				}
			}

			pal = colorize_create_palette(current, 0);
			(void) colorize_mandelbrot(buffer, current, pal, false);

			if (output_file) {
				png_save_as(output_file, buffer, width, height);
				storage_save(current, pal, path);
			} else {
				char fname[MAX_FLEN];

				storage_generate_unique_filename(current, fname, MAX_FLEN, ".png");

				png_save_as(fname, buffer, width, height);

				storage_save(current, pal, NULL);
			}
			colorize_free(pal);

			saved_images++;
			if (!verbose && !quiet) {
				write(STDOUT_FILENO, ".", 1);
				fsync(STDOUT_FILENO);
			}

		}
	}

	mandelbrot_free(current);
	mandelbrot_free(master);
	mandelbrot_free(previous);
	free(buffer);

	if (verbose)
		printf(" -- Done generating %d images\n", num_images);
	else if (!quiet)
		printf("done\n");
	return 0;
}
