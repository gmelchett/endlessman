/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017-2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <zlib.h>

#include "storage.h"
#include "mandelbrot.h"

#define MAX_FLEN 128

static unsigned int storage_generate_hash(struct mandelbrot *m)
{
	double data[4];

	data[0] = m->width;
	data[1] = m->height;
	data[2] = m->x;
	data[3] = m->y;

	return adler32(0, (unsigned char *)data, sizeof(data));
}

void storage_generate_unique_filename(struct mandelbrot *m, char *fname, int maxflen, char *suffix)
{
	struct stat tmp;
	unsigned int ad32 = storage_generate_hash(m);
	int colnum = 0;

	do {
		snprintf(fname, maxflen, "mandelbrot-%02x%08x%s", colnum, ad32, suffix);
		colnum++;
	} while (stat(fname, &tmp) == 0);
}

bool storage_save(struct mandelbrot *m, struct palette *pal, char *path)
{
	char fname[MAX_FLEN];
	char fullname[2*MAX_FLEN];

	storage_generate_unique_filename(m, fname, MAX_FLEN, ".endl");

	if (path) {
	    snprintf(fullname, 2*MAX_FLEN-1, "%s/%s", path, fname);
	} else {
	    snprintf(fullname, 2*MAX_FLEN-1, "%s", fname);
	}
	FILE *fp = fopen(fullname, "wb");

	if (fp == NULL)
		return false;

	fwrite("ENDL", 1, 4, fp);

	fwrite(&m->width, 1, sizeof(double), fp);
	fwrite(&m->height, 1, sizeof(double), fp);
	fwrite(&m->x, 1, sizeof(double), fp);
	fwrite(&m->y, 1, sizeof(double), fp);
	fwrite(&m->max_iteration, 1, sizeof(double), fp);
	fwrite(&m->depth_min, 1, sizeof(double), fp);
	fwrite(&m->depth_max, 1, sizeof(double), fp);
	fwrite(&pal->f, 1, sizeof(double), fp);

	for (int i = 0 ; i < (pal->f + 2); i++) {
		fwrite(&pal->hsv[i].h, 1, sizeof(double), fp);
		fwrite(&pal->hsv[i].s, 1, sizeof(double), fp);
		fwrite(&pal->hsv[i].v, 1, sizeof(double), fp);
	}

	fclose(fp);
	return true;
}

bool storage_load(char *fname, struct mandelbrot *m, struct palette **pal)
{
	char header[4];
	FILE *fp = fopen(fname, "rb");

	if (fp == NULL)
		return false;

	fread(header, 4, 1, fp);

	if (header[0] != 'E' ||
	    header[1] != 'N' ||
	    header[2] != 'D' ||
	    header[3] != 'L')
		return false;

	fread(&m->width, 1, sizeof(double), fp);
	fread(&m->height, 1, sizeof(double), fp);
	fread(&m->x, 1, sizeof(double), fp);
	fread(&m->y, 1, sizeof(double), fp);
	fread(&m->max_iteration, 1, sizeof(double), fp);
	fread(&m->depth_min, 1, sizeof(double), fp);
	fread(&m->depth_max, 1, sizeof(double), fp);

	(*pal) = colorize_allocate_palette(m);
	fread(&(*pal)->f, 1, sizeof(double), fp);

	(*pal)->hsv = malloc(((*pal)->f + 2) * sizeof(struct hsv));

	for (int i = 0 ; i < ((*pal)->f + 2); i++) {
		fread(&(*pal)->hsv[i].h, 1, sizeof(double), fp);
		fread(&(*pal)->hsv[i].s, 1, sizeof(double), fp);
		fread(&(*pal)->hsv[i].v, 1, sizeof(double), fp);
	}

	return true;
}
