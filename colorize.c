/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017-2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "colorize.h"
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/param.h>

#define COLOR_SPREAD 2

struct rgb {
	unsigned char r, g, b;
};

static void hsv2rgb(struct rgb *rgb, float h, float s, float v)
{
	if (s <= 0.0) {
		rgb->r = rgb->g = rgb->b = nearbyint(255.0 * v);
		return;
	}

	int i = floor(h * 6.0);
	float f = h * 6.0 - (float)i;

	unsigned char p = nearbyint(255.0 * (v * (1.0 - s)));
	unsigned char q = nearbyint(255.0 * (v * (1.0 - s * f)));
	unsigned char t = nearbyint(255.0 * (v * (1.0 - s * (1.0 - f))));
	unsigned char v2 = nearbyint(255.0 * v);
	i = i % 6;

        if (i == 0) {
		rgb->r = v2;
		rgb->g = t;
		rgb->b = p;
	} else if (i == 1) {
		rgb->r = q;
		rgb->g = v2;
		rgb->b = p;
	} else if (i == 2){
		rgb->r = p;
		rgb->g = v2;
		rgb->b = t;
	} else if (i == 3) {
		rgb->r = p;
		rgb->g = q;
		rgb->b = v2;
	} else if (i == 4) {
		rgb->r = t;
		rgb->g = p;
		rgb->b = v2;
	} else {
		rgb->r = v2;
		rgb->g = p;
		rgb->b = q;
	}
}

static int interpolate_linear(int from, int to, int t, int d)
{
	return ((to - from) * t) / d + from;
}

static int interpolate_quadratic_ease_in(int from, int to, int t, int d) 
{
         float ft = (float)t / (float)d;
         return interpolate_linear(from, to, (int)((ft * ft) * (float)d), d);
}

static int interpolate_quadratic_ease_out(int from, int to, int t, int d)
{
         float ft = 1.0f - ((float)t / (float)d);
         return interpolate_linear(to, from, (int)((ft * ft) * (float)d), d);
}

static int interpolate_quadratic_ease_in_out(int from, int to, int t, int d)
{
         float ft = ((float)t / (float)d) * 2.0f;
         float middle = (float)(from + to) / 2.0f;

         if (ft <= 1.0f)
                 return interpolate_linear(from, (int)middle, (int)((ft * ft) * (float)d), d);
         ft = 2.0f - ft;

         return interpolate_linear(to,(int)middle, (int)((ft * ft) * (float)d), d);
}

struct palette *colorize_allocate_palette(struct mandelbrot *m)
{
	int len = m->depth_max - m->depth_min;

	struct palette *pal = malloc(sizeof(struct palette));
	pal->r = malloc((len + 1) * sizeof(unsigned char));
	pal->g = malloc((len + 1) * sizeof(unsigned char));
	pal->b = malloc((len + 1) * sizeof(unsigned char));
	pal->len = len;
	pal->hsv = NULL;
	pal->f = 0;

	return pal;
}

void colorize_paint_palette(struct palette *pal)
{
	struct rgb start_p = {0}, end_p = {0};
	int fstep = pal->len / pal->f;

	int pidx = 0;
	for (int i = 0; ; i++) {
		int l = fstep;

		if (i == 0) {
			l += pal->len % pal->f;
			hsv2rgb(&start_p, pal->hsv[i].h, pal->hsv[i].s, pal->hsv[i].v);
		} else {
			start_p = end_p;
		}

		hsv2rgb(&end_p, pal->hsv[i + 1].h, pal->hsv[i + 1].s, pal->hsv[i + 1].v);

		for (int j = 0; j < l && (pidx + j) <= pal->len; j++) {
			if (i == 0) {
				pal->r[pidx + j] = interpolate_quadratic_ease_out(start_p.r, end_p.r, j, l);
				pal->g[pidx + j] = interpolate_quadratic_ease_out(start_p.g, end_p.g, j, l);
				pal->b[pidx + j] = interpolate_quadratic_ease_out(start_p.b, end_p.b, j, l);
			} else if (i == pal->f) {
				pal->r[pidx + j] = interpolate_quadratic_ease_in(start_p.r, end_p.r, j, l);
				pal->g[pidx + j] = interpolate_quadratic_ease_in(start_p.g, end_p.g, j, l);
				pal->b[pidx + j] = interpolate_quadratic_ease_in(start_p.b, end_p.b, j, l);
			} else {
				pal->r[pidx + j] = interpolate_quadratic_ease_in_out(start_p.r, end_p.r, j, l);
				pal->g[pidx + j] = interpolate_quadratic_ease_in_out(start_p.g, end_p.g, j, l);
				pal->b[pidx + j] = interpolate_quadratic_ease_in_out(start_p.b, end_p.b, j, l);
			}
		}
		if (pidx >= pal->len)
			break;
		pidx += l;
	}

}

struct palette *colorize_create_palette(struct mandelbrot *m, int nr_colors)
{
	struct palette *pal;
	int f;

	pal = colorize_allocate_palette(m);

	if (nr_colors)
		f = nr_colors;
	else
		f = MIN(random() % 2 + 3, pal->len / 20);

	pal->hsv = malloc((f + 2) * sizeof(struct hsv));
	pal->f = f;

	/* Either first or last entry is bright */
	int first_bright = random() % 2;

	for (int i = 0; i < (f + 2); i++) {
		do {
			pal->hsv[i].h = (float)random()/(float)RAND_MAX;
		} while (i > 0 && fabs(pal->hsv[i].h - pal->hsv[i - 1].h) < 0.15);

		pal->hsv[i].s = (float)random() / (float)RAND_MAX;

		if (i == 0 || i == f) {
			if ((first_bright && i == 0) || (!first_bright && i == f)) {
				do {
					pal->hsv[i].v = (float)random() / (float)RAND_MAX;
				} while (pal->hsv[i].v < 0.7);
			} else {
				do {
					pal->hsv[i].v = (float)random() / (float)RAND_MAX;
				} while (pal->hsv[i].v > 0.3);
			}
		} else {
			do {
				pal->hsv[i].v = (float)random() / (float)RAND_MAX;
			} while (pal->hsv[i].v < 0.15);
		}
	}

	colorize_paint_palette(pal);

	return pal;
}

void colorize_free(struct palette *pal)
{
	free(pal->r);
	free(pal->g);
	free(pal->b);
	if (pal->hsv)
		free(pal->hsv);
	free(pal);
}

static unsigned char linear(int a, int b, double i)
{
       return a + (unsigned char)(((double)(b - a)) * (i - floor(i)));
}

static void approx_color(unsigned char *buffer, struct palette *p, double i)
{
       buffer[0] = linear(p->r[(int)floor(i)], p->r[(int)floor(i) + 1], i);
       buffer[1] = linear(p->g[(int)floor(i)], p->g[(int)floor(i) + 1], i);
       buffer[2] = linear(p->b[(int)floor(i)], p->b[(int)floor(i) + 1], i);
}

static int peak_color_usage(unsigned int *h, unsigned int len) 
{
	int max_usage = 0;

	for (int i = 0; i < len; i++) {
		int u = 0;
		int c = -COLOR_SPREAD;

		if (i + c < 0)
			c = 0;
		if (i - c >= len)
			c -= (i - c) - len + 1;

		for (int j = 0; j < (2*COLOR_SPREAD); j++) {
			if (i+j+c >= len)
				printf("****** outside! i: %d j: %d c:%d\n", i, j, c);
			u += h[i+j+c];
		}
		if (u > max_usage)
			max_usage = u;
	}
	return max_usage;
}

static void draw_histogram(unsigned char *buffer, int width, int height, int x, int y, 
			   unsigned int *histogram, unsigned int start, unsigned int len, unsigned int extreme)
{
	int peak = 0;

	for (int i = start; i < len; i++) {
		if (peak < histogram[i])
			peak = histogram[i];
	}

	if (peak == 0)
		return;

	for (int i = start; i < (start+len)*2; i++) {
		for (int j = 0; j < 50; j++) {
			int w = 128;
			int b = 0;

			if (histogram[i/2] > width*height/100)
				w = 255;
			else if (histogram[i/2])
				w = 192;

			if (i/2 == extreme)
				b = 255;

			if ((50 - 50*histogram[i/2]/peak) < j) {
				buffer[width*y*3+j*width*3+(x+i-start)*3+0] = b;
				buffer[width*y*3+j*width*3+(x+i-start)*3+1] = 0;
				buffer[width*y*3+j*width*3+(x+i-start)*3+2] = 0;
			} else {
				buffer[width*y*3+j*width*3+(x+i-start)*3+0] = w;
				buffer[width*y*3+j*width*3+(x+i-start)*3+1] = w;
				buffer[width*y*3+j*width*3+(x+i-start)*3+2] = w;
			}
		}
	}
}

int colorize_mandelbrot(unsigned char *buffer, struct mandelbrot *m, struct palette *pal, bool info)
{
	unsigned int *gray;
	unsigned int *color;
	int idx = 0;
	int uncertain = 0;

	gray = malloc((PAL_LEN+1)*sizeof(unsigned int));
	memset((void*)gray, 0, (PAL_LEN+1)*sizeof(unsigned int));
	color = malloc((m->max_iteration+1)*sizeof(unsigned int));
	memset((void*)color, 0, sizeof(unsigned int)*(m->max_iteration+1));

	for (int h = 0; h < m->image_height; h++) {
		for (int w = 0; w < m->image_width; w++) {
			double iteration = m->mandelbrot[h*m->image_width+w];
			if (iteration < m->max_iteration ) {
				approx_color(buffer + idx, pal, iteration - m->depth_min);
			} else {
				buffer[idx + 0] = pal->r[pal->len - 1];
				buffer[idx + 1] = pal->g[pal->len - 1];
				buffer[idx + 2] = pal->b[pal->len - 1];
			}

			color[(int)iteration]++;
			int g = ((int)buffer[idx + 0] + (int)buffer[idx + 1] + (int)buffer[idx + 2]) / 3;
			if ((int)iteration == (int)m->max_iteration)
				uncertain = g;
			if (g >= PAL_LEN)
				printf("gray is outside: %d\n", g);
			gray[g]++;
			idx +=3;
		}
	}

	if (!info)
		goto out;

	draw_histogram(buffer, m->image_width, m->image_height, 0, 700, gray, 0, PAL_LEN, uncertain);
	draw_histogram(buffer, m->image_width, m->image_height, 0, 600, color, m->depth_min, pal->len, (int)m->max_iteration);

	for (int i = 0; i < PAL_LEN*2; i++) {
		for (int j = 0; j < 25; j++) {
			buffer[m->image_width*750*3+j*m->image_width*3+i*3+0] = i/2;
			buffer[m->image_width*750*3+j*m->image_width*3+i*3+1] = i/2;
			buffer[m->image_width*750*3+j*m->image_width*3+i*3+2] = i/2;
		}
	}

	for (int i = 0; i < pal->len*2; i++) {
		for (int j = 0; j < 25; j++) {
			buffer[m->image_width*650*3+j*m->image_width*3+(i+m->depth_min)*3+0] = pal->r[i/2];
			buffer[m->image_width*650*3+j*m->image_width*3+(i+m->depth_min)*3+1] = pal->g[i/2];
			buffer[m->image_width*650*3+j*m->image_width*3+(i+m->depth_min)*3+2] = pal->b[i/2];
		}
	}

out:;
	int g =  peak_color_usage(gray, PAL_LEN);
	free(gray);
	free(color);
	return g;
}
