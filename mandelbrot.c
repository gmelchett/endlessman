/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <sys/param.h>
#include <pthread.h>
#include <unistd.h>

#include "mandelbrot.h"

#define IMAGE_AREAS 3

struct histogram {
	unsigned int *areas[IMAGE_AREAS][IMAGE_AREAS];
	unsigned int usage[IMAGE_AREAS][IMAGE_AREAS];
	unsigned int *full;
	unsigned int full_usage;
};

struct mandelbrot_per_core {
	struct histogram *histogram;
	struct mandelbrot *m;
	int start_h;
	int end_h;
	pthread_t thread;
	pthread_mutex_t *lock;
};

static struct histogram *histogram_alloc(struct mandelbrot *m)
{
	struct histogram *h = malloc(sizeof(struct histogram));
	memset((void *)h, 0, sizeof(struct histogram));

	h->full = malloc(sizeof(unsigned int) * (m->max_iteration + 1));
	memset((void *)h->full, 0, sizeof(unsigned int) * (m->max_iteration + 1));

	for (int i = 0; i < IMAGE_AREAS; i++) {
		for (int j = 0; j < IMAGE_AREAS; j++) {
			h->areas[i][j] = malloc(sizeof(unsigned int) * (m->max_iteration + 1));
			memset((void *)h->areas[i][j], 0, sizeof(unsigned int) * (m->max_iteration + 1));
		}
	}
	return h;
}

static void histogram_free(struct histogram *h)
{
	for (int i = 0; i < IMAGE_AREAS; i++) {
		for (int j = 0; j < IMAGE_AREAS; j++) {
			free(h->areas[i][j]);
		}
	}

	free(h->full);
	free(h);
}

static bool is_interesting(struct mandelbrot *m, struct histogram *h)
{
	if (m->depth_max == m->depth_min)
		return false;

	m->dull = 0;
	for (int yy = 0; yy < IMAGE_AREAS; yy++) {
		for (int xx = 0; xx < IMAGE_AREAS; xx++) {
			if (100*h->usage[yy][xx]/m->max_iteration  < 20)
				m->dull++;
		}
	}

	return m->dull < 4 && (100 * h->full_usage / (m->depth_max - m->depth_min)) > 50;
}

static bool mandelbrot_core(struct mandelbrot *m, double px, double py, double *iteration, double *x, double *y)
{
	bool never = false;
	do {
		double xtemp = (*x)*(*x) - (*y)*(*y) + px;
		double ytemp = 2.0*(*x)*(*y) + py;
		if (xtemp == (*x) && ytemp == (*y)) {
			(*iteration) = m->max_iteration;
			m->never++;
			never = true;
			break;
		}
		(*x) = xtemp;
		(*y) = ytemp;
		(*iteration) += 1.0;

	} while (((*x)*(*x) + (*y)*(*y)) < 4.0 && ((*iteration) < m->max_iteration));

	if ((*iteration) < m->max_iteration ) {
		double log_zn = log((*x)*(*x) + (*y)*(*y)) / 2;
		double nu = log(log_zn / log(2)) / log(2);
		(*iteration) = (*iteration) + 1.0 - nu;
	}

	if (m->depth_min > floor(*iteration))
		m->depth_min = floor(*iteration);
	if (m->depth_max < floor(*iteration))
		m->depth_max = floor(*iteration);

	return never;
}

void mandelbrot_increase_depth(struct mandelbrot *m)
{
	struct mandelbrot_uncertain *u = malloc(m->image_width * m->image_height * sizeof(struct mandelbrot_uncertain));
	unsigned n = 0;

	for (int i = 0; i < m->num_uncertain; i++) {
		bool never = mandelbrot_core(m, m->u[i].px, m->u[i].py,
					     &m->u[i].iteration,
					     &m->u[i].x, &m->u[i].y);
		m->mandelbrot[m->u[i].idx] = MIN(m->u[i].iteration, m->max_iteration);
		if (!never && m->u[i].iteration >= m->max_iteration) {
			u[n] = m->u[i];
			n++;
		}
	}
	free(m->u);
	m->u = u;
	m->num_uncertain = n;
}

static void *mandelbrot_thread(void *data)
{
	struct mandelbrot_per_core *mpc = (struct mandelbrot_per_core *)data;
	struct mandelbrot *m = mpc->m;

	int idx = mpc->start_h * m->image_width;

	for (int h = mpc->start_h; h < mpc->end_h; h++) {
		double py = m->y + h*m->height/m->image_height;

		for (int w = 0; w < m->image_width; w++) {
			double px = m->x +w*m->width/m->image_width;
			double x = 0.0;
			double y = 0.0;
			double iteration = 0.0;

			bool never = mandelbrot_core(m, px, py, &iteration, &x, &y);

			pthread_mutex_lock(mpc->lock);
			if (mpc->histogram->areas[IMAGE_AREAS*w/m->image_width][IMAGE_AREAS*h/m->image_height][(int)iteration] == 0)
				mpc->histogram->usage[IMAGE_AREAS*w/m->image_width][IMAGE_AREAS*h/m->image_height]++;

			mpc->histogram->areas[IMAGE_AREAS*w/m->image_width][IMAGE_AREAS*h/m->image_height][(int)iteration]++;

			if (mpc->histogram->full[(int)iteration] == 0)
				mpc->histogram->full_usage++;
			mpc->histogram->full[(int)iteration]++;

			if (iteration < m->max_iteration) {
				if (iteration > 20*m->max_iteration/100) {
					m->p[m->p_len].x = px;
					m->p[m->p_len].y = py;
					m->p[m->p_len].iteration = iteration;
					m->p_len++;
				}
			} else if (!never) {
				m->u[m->num_uncertain].x = x;
				m->u[m->num_uncertain].y = y;
				m->u[m->num_uncertain].px = px;
				m->u[m->num_uncertain].py = py;
				m->u[m->num_uncertain].idx = idx;
				m->u[m->num_uncertain].iteration = floor(iteration);
				m->num_uncertain++;
			}
			pthread_mutex_unlock(mpc->lock);

			m->mandelbrot[idx] = MIN(iteration, m->max_iteration);

			idx += 1;
		}
	}
	return NULL;
}

void mandelbrot(struct mandelbrot *m)
{
	int cores = sysconf(_SC_NPROCESSORS_ONLN);
	struct histogram *histogram;
	struct mandelbrot_per_core *mpc;
	pthread_mutex_t lock;

	mpc = malloc(sizeof(struct mandelbrot_per_core) * cores);

	histogram = histogram_alloc(m);

	m->num_uncertain = 0;
	m->p_len = 0;
	m->depth_min = UINT_MAX;
	m->depth_max = 0;
	m->ok = false;
	m->never = 0;

	pthread_mutex_init(&lock, NULL);

	for (int i = 0; i < cores; i++) {
		mpc[i].m = m;
		mpc[i].histogram = histogram;
		mpc[i].start_h = (m->image_height / cores) * i;
		mpc[i].end_h = (m->image_height / cores) * (i + 1);
		mpc[i].lock = &lock;
		pthread_create(&mpc[i].thread, NULL, mandelbrot_thread, &mpc[i]);
	}
	mpc[cores - 1].end_h = m->image_height; /* In case height % cores != 0 */

	for (int i = 0; i < cores; i++) {
		pthread_join(mpc[i].thread, NULL);
	}

	pthread_mutex_destroy(&lock);

	struct mandelbrot_point *p = malloc(m->image_width * m->image_height * sizeof(struct mandelbrot_point));

	int j = 0;
	int d = m->depth_max - m->depth_min;
	for (int i = 0; i < m->p_len; i++) {
		if (100 * (m->p[i].iteration - m->depth_min) / d  > 25) {
			p[j] = m->p[i];
			j++;
		}
	}
	free(m->p);
	m->p = p;
	m->p_len = j;

	m->ok = is_interesting(m, histogram);

	histogram_free(histogram);
	free(mpc);
}

struct mandelbrot *mandelbrot_alloc(int image_width, int image_height)
{
	struct mandelbrot *m = malloc(sizeof(struct mandelbrot));

	if ((MAND_DEF_WIDTH / MAND_DEF_HEIGHT) > ((float)image_width / (float)image_height)) {
		m->width = MAND_DEF_WIDTH;
		m->height = MAND_DEF_HEIGHT * (MAND_DEF_WIDTH / MAND_DEF_HEIGHT) / ((float)image_width / (float)image_height);
		m->x = MAND_DEF_X;
		m->y = MAND_DEF_Y * m->height / MAND_DEF_HEIGHT;
	} else {
		m->width = MAND_DEF_WIDTH * ((float)image_width / (float)image_height) / (MAND_DEF_WIDTH / MAND_DEF_HEIGHT);
		m->height = MAND_DEF_HEIGHT;
		m->x = MAND_DEF_X * m->width / MAND_DEF_WIDTH;
		m->y = MAND_DEF_Y;
	}

	m->image_width = image_width;
	m->image_height = image_height;
	m->ok = false;
	m->depth_min = UINT_MAX;
	m->depth_max = 0;
	m->start_iteration = 0;
	m->max_iteration = DEFAULT_MAX_ITERATIONS;
	m->p_len = 0;
	m->dull = 0;
	m->num_uncertain = 0;
	m->p = malloc(image_width * image_height * sizeof(struct mandelbrot_point));
	m->u = malloc(image_width * image_height * sizeof(struct mandelbrot_uncertain));
	m->mandelbrot = malloc(image_width * image_height * sizeof(double));
	return m;
}

void mandelbrot_free(struct mandelbrot *m)
{
	free(m->p);
	free(m->u);
	free(m->mandelbrot);
	free(m);
}
