# Endless Mandelbrot Fractal Generator

This is a simple Mandelbrot fractal generator. It tries to generate subsets of the Mandelbrot fractal that
look pretty. It is not optimized for performance.

## Prerequisits

* libpng
* libSDL2 (for endlessman-show)

## How to build
Run `make`

## How to run
Run `endlessman`. It will output Mandelbrot images in png-format named `mandelbrot-#.png` (where # is the adler32 of some of the mandelbrot data) in your current directory until you kill it or
you run out of disk space. Default output resolution is 1920 x 1080. By default `endlessman` will also output a `.endl` file for each generated fractal.
That file includes enough information to regenerate the fractal.

    Usage: ./endlessman [options]

     -i n           Max iteration depth. Default is 150.
     -n n           Number of images to generate. Default no limit.
     -o <filename>  Generate one fractal (png + endl)
     -p             Run at highest nice level
     -q             Silence
     -s n           Random seed.
     -v             Verbose printing
     -x n           Image width. Default 1920
     -y n           Image height. Default 1080

Or you can run `endlessman-show` and it will show the mandelbrot fractals as they are generated in fullscreen mode.

* '1', '2', '3', '4', '5' Changes palette with 1 to 5 number of fixed colors
* 'i' shows some information about the current fractal.
* 'd' increases the interation depth with 50. A new palette will be generated as well.
* 's' saves the current image as `mandelbrot#.png`, where # is a number.
* 'e' saves the current image as `mandelbrot#.endl`. 
* 'q' Quit
* <space> Next image

It's not threaded, so there are no GUI updates while calculating.

## Thanks
* [James Jakobsson](https://github.com/slowcoder) for the interpolate algorithms.

## Examples

![alt text](examples/1.png "Mandelbrot fractal subset")
![alt text](examples/2.png "Mandelbrot fractal subset")
![alt text](examples/3.png "Mandelbrot fractal subset")
