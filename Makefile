CC=gcc
CFLAGS = -Wall -std=gnu99 -Ofast -g `pkg-config libpng sdl2 SDL2_ttf zlib --cflags`
LDFLAGS = -lm `pkg-config libpng sdl2 SDL2_ttf zlib --libs` -lpthread

all:  endlessman-show endlessman

endlessman: endlessman.o mandelbrot.o pngsave.o colorize.o storage.o
	$(CC) $^ $(LDFLAGS) -o $@

endlessman-show: endlessman-show.o mandelbrot.o pngsave.o colorize.o storage.o
	$(CC) $^ $(LDFLAGS) -o $@

clean:
	rm -f *.o endlessman endlessman-show *.endl *.png
