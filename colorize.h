/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdbool.h>
#include "mandelbrot.h"

#define PAL_LEN 256

struct hsv {
	double h, s, v;
};

struct palette {
	unsigned char *r;
	unsigned char *g;
	unsigned char *b;
	int len;
	struct hsv *hsv;
	int f;
};

struct palette *colorize_create_palette(struct mandelbrot *m, int nr_colors);
struct palette *colorize_allocate_palette(struct mandelbrot *m);
void colorize_paint_palette(struct palette *pal);

void colorize_free(struct palette *);
int colorize_mandelbrot(unsigned char *buffer, struct mandelbrot *m, struct palette *pal, bool info);
